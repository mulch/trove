! Copyright (C) 2022 wolf.

USING: accessors arrays assocs calendar.parser colors
combinators combinators.short-circuit combinators.smart
command-line continuations farkup io io.backend io.directories
io.encodings.utf8 io.files io.files.info io.files.types
io.monitors io.pathnames io.styles kernel make math math.order
namespaces present sequences sorting splitting strings
syndication trove.templates trove.types unicode
unicode.categories urls xml.writer ;
IN: trove

CATEGORY: slug-safe Ll Nd Zs ;

: slugify ( str -- str' )
    >case-fold nfd [ slug-safe? ] filter
    [ dup alpha? [ ] [ drop CHAR: - ] if ] map ;

: tell ( quot -- ) 
    "verbose" get
    [ H{ { font-style italic } { foreground COLOR: gray } } swap '[ ":: " write _ call ] with-style ] 
    [ drop ] if ; inline

: tell-error ( quot -- ) 
    H{ { font-style bold } { foreground COLOR: red } } swap '[ "!! " write _ call ] with-style ; inline

GENERIC: fix-links ( farkup -- farkup' )
M: array fix-links [ fix-links ] map ; recursive
M: tuple fix-links
    dup
    [ child>> ] [ drop drop { } ] recover
    [ fix-links ] map
    [ >>child ] [ drop drop ] recover ; recursive
M:: link fix-links ( link -- link' )
    link href>> dup absolute-url? 
    [ ] [ slugify "base-url" get [ ".html" append >url ] dip swap derive-url present ] if
    :> href 
    link href>> absolute-url? [ [
        [ href "url" ,, link text>> "title" ,, ] H{ } make
        1array swap append 
    ] "page-links" swap change ] unless link href >>href ;
M: line fix-links ; 
M: line-break fix-links ; 
M: code fix-links ;
M: image fix-links ;
M: string fix-links ;

: starts-with? ( el seq -- ? ) first = ;
: hidden-pathname? ( pathname -- ? ) CHAR: . swap starts-with? ;
: list-directory ( pathname -- seq )
    directory-entries [ name>> hidden-pathname? not ] filter ;

: prepare-parse ( string -- metadata content links )
    "---" split1 dup [ [ split-lines [ empty? not ] filter ] dip ] [ 2drop { } "" ] if f ;

:: parse-value ( key value -- key value' )
    f :> value'!
    key { 
        { "date" [ value ymd>timestamp value'! ] }
        { "tags" [ value ", " split sift value'! ] }
        [ drop value value'! ]
    } case key value' ;

:: assoc>metadata ( assoc -- metadata )
    metadata new :> meta
    assoc [
        first2 swap {
            { "title" [ meta title<< ] }
            { "description" [ meta description<< ] }
            { "date" [ meta date<< ] }
            { "page" [ meta page<< ] }
            { "tags" [ meta tags<< ] }
            { "slug" [ meta slug<< ] }
        } case
    ] each meta ;

:: parse-metadata ( metadata content links -- metadata' content links )
    ! H{ } clone :> attrs
    metadata [ ": " split1 parse-value 2array ] map assoc>metadata
    [ title>> [ length zero? not ] [ slugify ] [ "" ] smart-if* ] keep swap >>slug
    content links ;

:: parse-content ( metadata content links -- metadata content' links' )
    content parse-farkup :> ast!
    [ ast fix-links ast! "page-links" get ] with-scope :> links
    metadata ast (write-farkup) links ;

:: scaffold-site ( -- )
    "trove.cfg" file-exists? [ [ "This directory already has a site configuration, aborting." print ] tell-error ] [
        "_site" "static"
        [ make-directory ] bi@
        "base-url = http://example.com/\nsite-title = My new blog\nverbose = f\n"
        "trove.cfg" utf8 [ write ] with-file-writer
        [ 
            "A new site has been scaffolded at " write
            current-directory get write
            "." print 
        ] tell
    ] if ;

: scaffold-site-at ( path -- )
    [ [ { [ file-exists? ] [ directory? ] } 1&& not ] [ make-directory ] [ ] smart-if* ]
    [ [ scaffold-site ] with-directory ] bi ;

: tell-file ( file -- file )
    [ dup "Writing file " write "`" "'" surround write "..." print ] tell ;

: clear-output ( -- ) 
    current-directory get "_site" append-path
    [ dup file-exists? [ delete-tree ] [ drop ] if ]
    [ make-directory ] bi ;  

: copy-static-files ( -- )
    [ "Copying static files to output directory..." print ] tell
    current-directory get
    [ "static" append-path ] [ "_site" append-path ] bi
    copy-tree-into ;

: get-text-files ( -- list )
    current-directory get list-directory [
        [ type>> +regular-file+ = ]
        [ name>> file-extension "txt" = ] bi and
    ] filter ;

: parse-post ( direntry -- post )
    name>> utf8 file-contents
    prepare-parse parse-metadata parse-content post boa ;

: write-post ( post -- ) 
    [ template-post ] [ post>path tell-file ] bi 
    utf8 [ write-xml ] with-file-writer ;

: write-index ( list -- ) 
    template-index "_site" "index.html" append-path tell-file
    utf8 [ write-xml ] with-file-writer ;

: write-feed ( list -- )
    [ post>rss-entry ] map
    [ "site-title" get "base-url" get ] dip
    feed boa feed>xml
    "_site" "atom.xml" append-path tell-file
    utf8 [ write-xml ] with-file-writer ;

: generate-site ( -- )
    clear-output
    copy-static-files
    get-text-files
    [ parse-post ] map
    [ [ write-post ] each ] [ 
        [ metadata>> page>> not ] filter
        [ [ metadata>> date>> ] bi@ >=< ] sort
        [ write-index ] [ write-feed ] bi 
    ] bi ;

:: (parse-config) ( -- cfg )
    H{ } clone :> cfg
    "trove.cfg" utf8 file-lines
    [ [let 
        "=" split1 [ [ CHAR: \s = ] trim ] bi@ swap
        {
            { "base-url" [ >url "base-url" cfg set-at ] }
            { "site-title" [ "site-title" cfg set-at ] }
            { "verbose" [ "f" = f t ? "verbose" cfg set-at ] }
        } case
    ] ] each cfg ;

: parse-config ( -- cfg )
    [ (parse-config) ] [ 
        drop [
            "This directory does not have a proper site configuration, aborting."
            print
        ]
        tell-error return
    ] recover ;

: with-config ( quot -- )
    '[ _ parse-config swap with-variables ] with-return ; inline

: with-dev-config ( quot -- )
    '[ _ URL" http://localhost:8000" "base-url"
        parse-config [ set-at ] keep
        [ t "verbose" ] dip [ set-at ] keep
    swap with-variables ] with-return ; inline

: trove-help ( -- )
    "Usage: trove [ build | build-dev | watch | scaffold <path> ]" print nl
    "Subcommands: " print
    "    build:           build site in current working directory" print
    "    build-dev:       build site (with development configuration) in current working directory" print
    "    watch:           watch current directory for changes and rebuild (assumes running with development configuration)" print
    "    scaffold <path>: scaffold new site in specified directory" print ;

: trove-main ( -- )
    command-line get empty?
    [ trove-help ]
    [
        command-line get first {
            { "scaffold"  [ command-line get second normalize-path scaffold-site-at ] }
            { "build"     [ [ generate-site ] with-config ] }
            { "build-dev" [ [ generate-site ] with-dev-config ] }
            [ [ "`" "'" surround write " is not a valid subcommand." print ] tell-error ]
        } case
    ] if ;

MAIN: trove-main

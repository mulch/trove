USING: accessors assocs combinators io.pathnames kernel
namespaces present sequences syndication urls ;
IN: trove.types

TUPLE: metadata title description date tags page slug ;
TUPLE: post metadata content links ;

: post>path ( post -- path ) 
    metadata>> slug>> "_site" swap append-path
    ".html" append ;

: post>url ( post -- url )
    "base-url" get 
    [ metadata>> slug>> ".html" append >url ] dip
    swap derive-url ;

:: post>rss-entry ( post -- entry ) 
    <entry> :> entry
    post {
        [ metadata>> title>> entry title<< ]
        [ metadata>> description>> entry description<< ]
        [ metadata>> date>> entry date<< ]
        [ post>url entry url<< ]
    } cleave entry ;

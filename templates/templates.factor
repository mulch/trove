! Copyright (C) 2022 wolf.
! See http://factorcode.org/license.txt for BSD license.

USING: trove.types accessors assocs calendar.format
combinators.short-circuit kernel namespaces sequences urls
xml.syntax ;
IN: trove.templates

: make-link ( title url -- xml )
    swap <XML <li><a href=<->><-></a></li> XML> ;

:: make-index-link ( post -- xml )
    post metadata>> title>> :> title
    post post>url :> href
    post metadata>> date>> :> timestamp
    timestamp timestamp>iso8601 :> date-string
    timestamp timestamp>ymd :> date-present
    post metadata>> description>> :> description!
    description dup [ <XML <span><br/><-></span> XML> description! ] [ drop ] if

    [XML <li>
    <strong><a href=<-href->><-title-></a></strong>:
    <em>published in <time datetime=<-date-string->><-date-present-></time></em>
    <-description->
</li> XML] ;

: make-self-link ( title url -- xml )
    swap <XML <li><a class="link-self" href=<->><-></a></li> XML> ;

:: template-head ( title -- xml )
    "site-title" get :> site-title

    [XML <head>
    <meta charset="UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title><-site-title-> - <-title-></title>
    <link rel="stylesheet" href="/static/style.css"/>
    <link rel="alternate" type="application/rss+xml" href="/atom.xml" />
</head> XML] ;

:: template-header ( post -- xml )
    "base-url" get :> base-url
    base-url URL" index.html" derive-url :> index-url
    base-url URL" contact.html" derive-url :> contact-url
    base-url URL" atom.xml" derive-url :> atom-url
    "site-title" get :> site-title
    
    post { [ ] [ metadata>> page>> not ] } 1&& [
        post metadata>> title>>
        post post>url
        make-self-link
        post links>> [
            dup [ "title" swap at ] dip "url" swap at
            make-link
        ] map
        <XML <ul><-><-></ul> XML>
    ] [ f ] if :> links

    [XML <header>
    <span class="site-title"><-site-title-></span>
    <nav>
        <ul>
            <li><a href=<-index-url->>Home</a></li>
            <li><a href=<-contact-url->>Contact</a></li>
            <li><a href=<-atom-url->>RSS feed</a></li>
        </ul>
        <-links->
    </nav>
</header> XML] ;

:: template-footer ( -- xml )
    <XML <footer>
    The content for this site is <a class="ext-link" rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/deed.en" target="_blank">CC BY-SA 4.0</a>. The code used to generate site is under the <a class="ext-link" rel="license" href="https://opensource.org/licenses/ISC" target="_blank">ISC license</a>, and can be seen <a href="https://codeberg.org/wrwlf/trove" class="ext-link" target="_blank">here</a>.
</footer> XML> ;

:: template-post ( post -- xml )
    post metadata>> title>>     :> title
    post content>>                    :> content
    "base-url" get                    :> index-url
    post post>url                     :> post-url
    post links>> [ [ "title" swap at ] [ "url" swap at ] bi@ make-link ] map :> links!
    post template-header              :> header
    template-footer :> footer
    title template-head               :> head

    [XML <html>
    <-head->
    <body>
        <-header->
        <hr/>
        <main><-content-></main>
        <-footer->
    </body>
</html> XML] ;

:: template-index ( posts -- xml )
    f template-header :> header
    template-footer :> footer
    "Home" template-head :> head
    "base-url" get :> index-url
    posts [ make-index-link ] map sift :> post-links

    [XML <html>
    <-head->
    <body>
        <-header->
        <hr/>
        <main>
            <p>Welcome to my den. I like writing code and I try to write words: sometimes about the code I write, sometimes about random things I think about.</p>

            <h3>Posts:</h3>
            <ul class="posts"><-post-links-></ul>
        </main>
        <-footer->
    </body>
</html> XML] ;
